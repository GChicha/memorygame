/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from "react";

import { createAppContainer, createStackNavigator } from "react-navigation";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import thunkMiddleware from "redux-thunk";

import rootReducer from "./reducers";
import Home from "./pages/Home";
import Game from "./pages/Game";
import NavigationService from "./services/NavigationService";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer,
  undefined,
  composeEnhancers(applyMiddleware(thunkMiddleware))
);

const AppNavigator = createStackNavigator(
  { Home, Game },
  { initialRouteName: "Home" }
);

const AppContainer = createAppContainer(AppNavigator);

export default () => {
  return (
    <Provider store={store}>
      <AppContainer
        ref={navigationRef =>
          NavigationService.setTopLevelNavigator(navigationRef)
        }
      />
    </Provider>
  );
};
