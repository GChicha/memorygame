// @flow

import AsyncStorage from "@react-native-community/async-storage";

export async function getRank(): Promise<Array<{ username: string, rounds: number }>> {
  try {
    return JSON.parse(await AsyncStorage.getItem("rank"));
  } catch (e) {
    return [];
  }
}

export async function addToRank(username: string, rounds: number) {
  await AsyncStorage.setItem(JSON.stringify({ username, rounds }));
}
