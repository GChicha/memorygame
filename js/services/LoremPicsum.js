// @flow

export async function getImageUrls(
  width: number,
  height: number
): Promise<Array<string>> {
  const url = `https://picsum.photos/v2/list?page=${(
    Math.random() * 10
  ).toFixed(0)}&limit=10`;
  console.log(url);
  const imageList: Array<{ id: string, url: string }> = await (await fetch(
    `https://picsum.photos/v2/list?page=${(Math.random() * 10).toFixed(
      0
    )}&limit=10`
  )).json();

  const ids = imageList.map(image => image.id);

  return ids.map(id => `https://picsum.photos/id/${id}/${width}/${height}`);
}
