// @flow

import { getRank } from "../services/Ranking";

type State = {
  fetching: boolean,
  ranking: Array<{ username: string, rounds: number }>
};

const INITIAL_STATE = {
  fetching: false,
  ranking: []
};

type SetRanking = {
  type: "SET_RANKING",
  ranking: Array<{ username: string, rounds: number }>
};

type StartFetching = {
  type: "START_FETCHING"
};

type FinishFetching = {
  type: "FINISH_FETCHING"
};

type Action = SetRanking | StartFetching | FinishFetching;

export default function reducer(
  state: State = INITIAL_STATE,
  action: Action
): State {
  switch (action.type) {
    case "SET_RANKING":
      return { ...state, ranking: action.ranking };
    case "START_FETCHING":
      return { ...state, fetching: true };
    case "FINISH_FETCHING":
      return { ...state, fetching: false };
    default:
      return INITIAL_STATE;
  }
}

function setRanking(
  ranking: Array<{ username: string, rounds: number }>
): SetRanking {
  return {
    type: "SET_RANKING",
    ranking
  };
}

function startFetching(): StartFetching {
  return {
    type: "START_FETCHING"
  };
}

function finishFetchimg(): FinishFetching {
  return {
    type: "FINISH_FETCHING"
  };
}

export function fetchRanking() {
  return async dispatch => {
    dispatch(startFetching());
    const ranking = await getRank();
    dispatch(setRanking(ranking));
    dispatch(finishFetchimg());
  };
}
