//@flow

import { getImageUrls } from "../services/LoremPicsum";

type State = {
  +username: string,
  +cards: Array<string>,
  +cardState: Array<Map<string, boolean>>,
  +round: number,
  +fetching: boolean
};

const INITIAL_STATE: State = {
  username: "",
  cards: [],
  cardState: [],
  round: 0,
  fetching: false
};

// Actions

type SetUsername = {
  type: "SET_USERNAME",
  username: string
};

type SetCards = {
  type: "SET_CARDS",
  cards: Array<string>
};

type SetCardState = {
  type: "SET_CARD_STATE",
  cardState: Array<Map<string, boolean>>
};

type OpenCard = {
  type: "OPEN_CARD",
  index: number
};

type IncrementRound = {
  type: "INCREMENT_ROUND"
};

type ResetState = {
  type: "RESET"
};

type StartFetching = {
  type: "IMAGE_LIST_START_FETCHING"
};

type FinishFetching = {
  type: "IMAGE_LIST_FINISH_FETCHING"
};

type Action =
  | SetUsername
  | OpenCard
  | SetCards
  | SetCardState
  | IncrementRound
  | StartFetching
  | FinishFetching
  | ResetState;

export default function reducer(
  state: State = INITIAL_STATE,
  action: Action
): State {
  switch (action.type) {
    case "SET_USERNAME":
      return { ...state, username: action.username };
    case "OPEN_CARD":
      const index = action.index;
      const id = Object.keys(state.cardState[index])[0];

      if (!state.cardState[index][id]) {
        if (state.round % 2 == 0) {
          for (let i = 0; i < state.cardState.length; i += 2) {
            const id_loop = Object.keys(state.cardState[i])[0];
            const id2_loop = Object.keys(state.cardState[i + 1])[0];

            if (
              state.cardState[i][id_loop] != state.cardState[i + 1][id2_loop]
            ) {
              state.cardState[i][id_loop] = false;
              state.cardState[i + 1][id2_loop] = false;
            }
          }
        }

        state.cardState[index][id] = true;
        state.round += 1;
      }
      return state;
    case "SET_CARDS":
      return {
        ...state,
        cards: action.cards
      };
    case "SET_CARD_STATE":
      return {
        ...state,
        cardState: action.cardState
      };
    case "RESET":
      return INITIAL_STATE;
    case "IMAGE_LIST_START_FETCHING":
      return { ...state, fetching: true };
    case "IMAGE_LIST_FINISH_FETCHING":
      return { ...state, fetching: false };
    default:
      return state;
  }
}

export function setUsername(username: string): SetUsername {
  return {
    type: "SET_USERNAME",
    username
  };
}

export function getUsername(state: State): string {
  return state.username;
}

export function openCard(index: number): OpenCard {
  return { type: "OPEN_CARD", index };
}

export function getCards(state: State): Array<string> {
  return state.cards;
}

function startFetching(): StartFetching {
  return {
    type: "IMAGE_LIST_START_FETCHING"
  };
}
function finishFetching(): FinishFetching {
  return {
    type: "IMAGE_LIST_FINISH_FETCHING"
  };
}

export function reset(): ResetState {
  return {
    type: "RESET"
  };
}

function setCards(cards: Array<string>): SetCards {
  return {
    type: "SET_CARDS",
    cards
  };
}

function setCardState(cardState: Array<Map<string, boolean>>): SetCardState {
  return {
    type: "SET_CARD_STATE",
    cardState
  };
}

export function getShuffled(state: State): Array<string> {
  return state.cardState
    .map(cardState => Object.keys(cardState))
    .reduce((acc, current) => acc.concat(current), []);
}

export function getVisibility(state: State): Array<boolean> {
  return state.cardState
    .map(cardState => Object.values(cardState))
    .reduce((acc, current) => acc.concat(current), []);
}

export function getFetching(state: State): boolean {
  return state.fetching;
}

export function startGame(username: string, cardNumber: number) {
  return dispatch => {
    dispatch(reset());
    dispatch(setUsername(username));
    const range = n => Array.from(Array(n).keys());
    const cardState = range(cardNumber)
      .sort(() => Math.random() - 0.5)
      .map(n => ({ [n]: false }));
    console.log(cardState);
    dispatch(setCardState(cardState));
  };
}

export function fetchCards(width: number, height: number) {
  return async dispatch => {
    dispatch(startFetching());
    const imageUrls = await getImageUrls(width, height);
    dispatch(setCards(imageUrls));
    dispatch(finishFetching());
  };
}
