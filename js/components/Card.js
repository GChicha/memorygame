// @flow

import React from "react";
import { View, StyleSheet, TouchableWithoutFeedback } from "react-native";
import { Card as CardElement, Image } from "react-native-elements";

type Props = {
  imageUrl: string,
  visible: boolean
};

export default function Card(props: Props) {
  return (
    <View style={style.card}>
      {(props.visible && (
        <Image
          resizeMode="cover"
          style={style.image}
          source={{ uri: props.imageUrl }}
        />
      )) || (
        <Image
          resizeMode="cover"
          style={style.image}
          source={{
            uri:
              process.env.REACT_APP_CARD_BACK ||
              "http://res.freestockphotos.biz/pictures/15/15687-illustration-of-a-play-card-back-pv.png"
          }}
        />
      )}
    </View>
  );
}

const style = StyleSheet.create({
  card: {
    width: "20%",
    height: "10%"
  },
  image: {
    width: 40,
    height: 40
  }
});
