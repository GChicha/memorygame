// @flow

import React from "react";
import { View, Text } from "react-native";

function RankingItem(props: { username: string, rounds: number }) {
  return <Text>{`{username}: {rounds}`}</Text>;
}

export function Ranking(props: {
  items: Array<{ username: string, rounds: number }>
}) {
  return (
    <View>
      {props.items
        .sort((a, b) => b.rounds - a.rounds)
        .map(item => (
          <RankingItem username={item.username} rounds={item.rounds} />
        ))}
    </View>
  );
}

export default Ranking;
