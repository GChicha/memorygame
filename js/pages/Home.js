// @flow
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { View, Text, TextInput, StyleSheet } from "react-native";
import { Input, Button } from "react-native-elements";

import NavigationService from "../services/NavigationService";
import { fetchCards, getFetching, getCards, startGame } from "../reducers/game";

type Props = {
  fetchingCards: boolean,
  cards: Array<string>,
  fetchCards: (number, number) => void,
  startGame: (string, number) => void
};

export function Home(props: Props) {
  const [userName, setUserName] = useState("");

  useEffect(() => {
    if (props.cards.length == 0) {
      props.fetchCards(200, 200);
    }
  });

  return (
    <View>
      <Input
        placeholder="Username"
        value={userName}
        onChangeText={setUserName}
      />
      <Button
        disable={props.fetchingCards || props.cards.length == 0}
        onPress={() => {
          props.startGame(userName, props.cards.length * 2);
          NavigationService.navigate("Game");
        }}
        title="Go"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  page: {
    backgroundColor: "#000"
  }
});

const mapStateToProps = state => ({
  cards: getCards(state.game),
  fetchingCards: getFetching(state.game)
});
const mapDispatchToProps = {
  fetchCards,
  startGame
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
