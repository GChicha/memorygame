// @flow

import React, { useState, useEffect } from "React";
import { connect } from "react-redux";
import { View, StyleSheet, TouchableWithoutFeedback } from "react-native";

import Card from "../components/Card";
import NavigationService from "../services/NavigationService";
import {
  getCards,
  getShuffled,
  getVisibility,
  startGame,
  fetchCards,
  openCard
} from "../reducers/game";

type Props = {
  cards: Array<string>,
  shuffled: Array<number>,
  visibility: Array<boolean>,
  openCard: number => void
};

export function Game(props: Props) {
  useEffect(() => {});

  return (
    <View style={style.page}>
      {props.shuffled.map((item, index) => (
        <TouchableWithoutFeedback
          key={index}
          onPress={() => props.openCard(item)}
        >
          <Card
            visible={props.visibility[index]}
            imageUrl={props.cards[Math.floor(item / 2)]}
          />
        </TouchableWithoutFeedback>
      ))}
    </View>
  );
}

const style = StyleSheet.create({
  page: {
    alignItems: "flex-start",
    flexWrap: "wrap",
    flex: 1
  }
});

const mapStateToProps = state => ({
  cards: getCards(state.game),
  shuffled: getShuffled(state.game),
  visibility: getVisibility(state.game)
});
const mapDispatchToProps = {
  openCard
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Game);
